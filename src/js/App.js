import React, { useContext } from 'react'
import ShowEntries from './components/ShowEntries'
import ShowSingleEntry from './components/ShowSingleEntry'
import UploadEntry from './components/UploadEntry'
import { StoreContext } from './context/store'
import { Auth } from 'aws-amplify'
import { AmplifyAuthenticator } from '@aws-amplify/ui-react'


export default function App() {
	const { state, actions } = useContext(StoreContext)

	function signOut() {
		Auth.signOut()
	}

	return (
		<AmplifyAuthenticator>
			<div>
				<header>
					<span>
						{state.page === 'preview' && <button onClick={() => actions.page.setToUploadPage()}>Upload</button>}
						{state.page === 'upload' && <button onClick={() => actions.page.setToPreviewPage()}>Preview</button>}
					</span>
					<span>
						<button onClick={signOut}>Sign Out</button>
					</span>
				</header>
				{state.page === 'upload' && <UploadEntry />}
				{state.page === 'preview' && <ShowEntries />}
				{state.page === 'single' && <ShowSingleEntry />}
			</div >
		</AmplifyAuthenticator>
	)
}


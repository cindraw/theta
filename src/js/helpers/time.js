export function getTimeDiff(isoTime) {
	const dateNow = new Date()
	const dateCreated = new Date(isoTime)
	const millisecondsNow = dateNow.getTime()
	const millisecondsCreated = dateCreated.getTime()
	const timeDiff = ~~((millisecondsNow - millisecondsCreated) / 60000)
	return timeDiff
}


export function displayPrice(price) {
	if (price.length <= 3) return '£ ' + price
	const reversed = price.toString().split('').reverse().join('')
	let count = 0
	const limit = 3
	const res = []
	for (let i of reversed) {
		res.push(i)
		count++
		if (count === limit) {
			res.push(',')
			count = 0
		}
	}
	const result = res.reverse()
	if (result[0] === ',') result[0] = '' 
	return '£ ' + result.join('').toString()
}
import React, { useContext, useRef } from 'react'
import { StoreContext } from '../context/store'
import { DataStore } from '@aws-amplify/datastore'
import { Entry } from '../../models'
import { Storage } from 'aws-amplify'
import { v4 as uuidv4 } from 'uuid'
import domtoimage from 'dom-to-image'


export default function UploadEntry() {
	const { actions } = useContext(StoreContext)

	const textArea = useRef()
	const inputPrice = useRef()

	function saveToCloud() {
		const theDiv = document.getElementById('myImage')
		saveImage(theDiv)
	}

	function saveImage(theDiv) {
		domtoimage.toBlob(theDiv)
			.then(blob => storeImage(blob))
			.then(key => createDataEntry(key))
			.catch(err => console.error(err))
	}

	function createDataEntry(key) {
		const newEntry = {
			image: key,
			address: textArea.current.value,
			price: inputPrice.current.value,
			date: new Date().toISOString(),
		}
		createEntry(newEntry).then(el => {
			actions.page.setToPreviewPage()
		})
	}

	return (
		<div>
			<input
				id='input_file'
				type='file'
				accept='image/*'
				onChange={loadImage}
				style={{ visibility: 'hidden' }}
			/>

			<div className='uploadpage'>
				<div onClick={triggerClick} className='imageholder' style={{ position: 'relative' }}>
					<div id='myImage'></div>
				</div>

				<div className='inputsarea'>
					<span>
						<strong>Address :</strong>
						<textarea ref={textArea} cols='40' rows='14'></textarea>
					</span>

					<span>
						<strong>Price :</strong>
						<input
							ref={inputPrice}
							type='text'
						/>
					</span>

					<span>
						<button onClick={saveToCloud}>Save</button>
					</span>
				</div>
			</div>

		</div>
	)
}



async function createEntry(entry) {
	try {
		const saved = DataStore.save(
			new Entry({ ...entry })
		)
		if (saved.id) console.log(saved)
		return saved
	} catch (error) {
		console.error(error)
	}
}


async function storeImage(src) {
	const uniquename = uuidv4() + '.png'
	try {
		const image = await Storage.put(uniquename, src)
		return image.key
	} catch (error) {
		console.error(error)
	}
}


function loadImage(e) {
	const file = e.target.files[0]
	const reader = new FileReader()
	reader.onload = function () {
		const source = reader.result
		const img = new Image()
		img.src = source
		img.style.height = '400px'
		const holder = document.getElementById('myImage')
		holder.appendChild(img)
	}
	reader.readAsDataURL(file)
}


function triggerClick() {
	const inputfile = document.getElementById('input_file')
	inputfile.click()
}
import React, { useState, useEffect, useContext } from 'react'
import { StoreContext } from '../context/store'
import { displayPrice } from '../helpers/price'
import MyImage from './MyImage'



export default function ShowSingleEntry() {
	const { state, actions } = useContext(StoreContext)
	const [Preview, setPreview] = useState(null)

	useEffect(() => {
		setPreview(state.preview)
	}, [state.preview])

	function closeSinglePage() {
		actions.page.setToPreviewPage()
	}

	return (
		<>
			{
				Preview &&
				<div className='single'>

					<button onClick={closeSinglePage}>{'<'} Back To List</button>

					<div className='holder'>

						<MyImage imgKey={Preview.image} />

						<div className='column'>
							
							<span>
								<strong>Address :</strong>
								<span>
									{
										Preview.address.split(',').map((el, i) => {
											return (
												<p key={i}>{el}</p>
											)
										})
									}
								</span>
							</span>

							<span>
								<strong>Price :</strong>
								<p>{displayPrice(Preview.price)}</p>
							</span>

						</div>

					</div>

				</div>
			}
		</>
	)

}


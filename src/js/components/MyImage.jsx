import React, { useState, useEffect } from 'react'
import { Storage } from 'aws-amplify'

export default function MyImage({ imgKey, type }) {
	const [ImgUrl, setImgUrl] = useState()

	useEffect(() => {
		getFromStorage(imgKey).then(el => setImgUrl(el))
	}, [imgKey])

	return (
		<>
			{!ImgUrl && <span>Loading...</span>}
			{ImgUrl && <img src={ImgUrl} alt={imgKey} className={type} />}
		</>
	)
}

async function getFromStorage(key) {
	try {
		const image = await Storage.get(key)
		return image
	} catch (error) {
		console.log(error)
	}
}
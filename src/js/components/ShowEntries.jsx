import React, { useState, useEffect, useContext } from 'react'
import { StoreContext } from '../context/store'
import { DataStore, Predicates, SortDirection } from '@aws-amplify/datastore'
import { Entry } from '../../models'
import { getTimeDiff } from '../helpers/time'
import { displayPrice } from '../helpers/price'
import MyImage from './MyImage'



export default function ShowEntries() {
	const { actions } = useContext(StoreContext)
	const [Images, setImages] = useState([])

	useEffect(() => {
		getData().then(el => {
			setImages(el)
		})
	}, [])

	return (
		<>
			<div className='allCards'>
				{
					Images.map((data, i) => {
						const timeDiff = getTimeDiff(data.date)
						return (
							<div className='card' key={i}>
								<MyImage imgKey={data.image} type={'thumbnail'}/>
								<p className='singleline'><strong>{data.address.split(',')[0]}</strong></p>
								<p>{displayPrice(data.price)}</p>
								<button
									style={{ width: 216, margin: '0 auto', marginLeft: 4 }}
									onClick={() => {
										actions.image.setPreviewedImage(data)
										actions.page.setToSinglePage()
									}}>
									View This Property {'>'}
								</button>
								<p className='time'>Last updated {timeDiff} minutes ago</p>
							</div>
						)
					}
					)
				}
			</div>
		</>
	)
}






async function getData() {
	try {
		const items = await DataStore.query(Entry, Predicates.ALL, {
			sort: s => s.date(SortDirection.DESCENDING)
		})
		return items
	}
	catch (error) {
		console.error(error)
	}
}



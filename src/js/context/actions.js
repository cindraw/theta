import { types } from './reducers'

export const useActions = (state, dispatch) => {

	return {

		image: {

			setPreviewedImage(image) {
				dispatch({ type: types.SET_PREVIEW, payload: image })
			},

			removePreviewImage() {
				dispatch({ type: types.SET_PREVIEW, payload: null })
			}

		},

		page: {

			setToUploadPage() {
				dispatch({ type: types.SET_PAGE, payload: 'upload' })
			},

			setToPreviewPage() {
				dispatch({ type: types.SET_PAGE, payload: 'preview' })
			},

			setToSinglePage() {
				dispatch({ type: types.SET_PAGE, payload: 'single' })
			}


		},


	}
}
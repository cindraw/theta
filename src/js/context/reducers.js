const initialState = {
	show_state_in_console: false,
	page: 'upload',
	preview: null
}

const types = {
	SET_PREVIEW: 'SET_PREVIEW',
	SET_PAGE: 'SET_PAGE'
}

const reducer = (state = initialState, action) => {

	switch (action.type) {

		case types.SET_PAGE:
			return {
				...state,
				page: action.payload
			}

		case types.SET_PREVIEW:
			return {
				...state,
				preview: action.payload
			}

		default:
			throw new Error('Unexpected Action')

	}


}

export { initialState, types, reducer }

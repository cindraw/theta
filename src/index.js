import React from 'react'
import ReactDOM from 'react-dom'
import App from './js/App'
import * as serviceWorker from './serviceWorker'
import { StoreProvider } from './js/context/store'
import './styles/_base.sass'

import Amplify from 'aws-amplify'
import config from './aws-exports'
Amplify.configure(config)


ReactDOM.render(
  <StoreProvider>
    <App />
  </StoreProvider>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://github.com/facebook/create-react-app/blob/master/packages/cra-template/template/README.md
serviceWorker.unregister()

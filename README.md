# Simple Interfaces

## How To
- click on grey area to upload images
- fill in the infos
- click save
- use buttons to navigate
- you can upload as many copies of the same image as you want. Images are processed and renamed locally before being sent off to S3 and a record is kept in the DB.


### Warning
~~If connection is shaky sometimes images upload as blank as if the backend needs to warm up. Keep uploading images. It ends up working out nicely.~~  

This is a dom-to-image issue.

## Tech
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).  
The backend is made with the [AWS Amplify framework](https://aws.amazon.com/amplify/) using GraphQL, S3, and Cognito.   

Access [Amplify Admin UI](https://eu-central-1.admin.amplifyapp.com/admin/login?appId=d2k265uevda3or&code=0b835827-8bb6-4992-a4dc-0c1f96e2b8ca&sessionId=e08a6f1c-9cd3-4c5a-9684-3ae8d9c5fb57&backendEnvironmentName=master) use credentials (top of page).   
Access [Amplify Console](https://eu-central-1.console.aws.amazon.com/amplify/home?region=eu-central-1#/d2k265uevda3or) use credentials (top of page).  


## About
I built this project with a template that I've been working on for about 15 months. It is coupled with a simple yet powerful cloud backend on AWS. This has been powering my developement for the past 9 months. So it's fresh in my mind and allows me to work very quickly.

The React template leverages useContext and useReducers hooks in a way that resembles classic Redux in it's structure. However unlike Redux the setup is minimal. 

The way src/js/context/... is setup is my downfall with regards to TypeScript. I haven't had a moment to take an honest creck at mutating my template to TypseScript. This is my immediate goal (for that matter I would greatly appreciate some help). It would allow me to finally fully adopt TypeScript in my workflow. Had I not needed context in this mini app, I would have made it using TypeScript.

## Additional improvements

#### Upload Images
Multiple images uploads.  
Add image editing capacities like a true image editor.  
Export JPG and compression settings. It's png now.  
More granular form fields, required, inputs types, and formatting goes a long way to ensure clean data.  

#### Process Thumbnails
There is only one size of images. Thus large images are downloaded in lieu of thumbnails. This is a hog on bandwidth that can easily be addressed at asset creation.

#### Loading Assets
When components appear on the DOM and the images aren't loaded, text elements will jump around. This could be made much smoother with additional css. 

Load spinners are a nice addition. As is, while assets are downloading, we stare into the white abyss and risk reflecting on our shortcomings.

#### UI components
A little more time spent pushing pixels around would make it look tight.
Follow Materials UI guidelines.  
Try some other fonts.  
Component libraries ? Materials UI ? Scrap that, hire a designer.  
display: inline-block behaves erratically. __*fixed with css*__

#### UX
Sync Datastore on page load.     
Use a navigation library like react-hooks-router that has some real world capacities and history.    
Upload and Download progress.    
Update entries.    

#### Reducers 
Extract Datastore and Storage calls to the reducers for better tracking.

#### Code
Extract spaghetti into pretty little functions with lovely return calls in TypeScript because that's what Bob Ross would do.

#### SEO
Server side rendering.